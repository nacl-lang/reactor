use std::io::prelude::*;
use std::env;
use std::fs::File;
mod vm;

fn main() {
    let args : Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("No input file provided.\n\tUsage: reactor FILENAME");
        std::process::exit(1);
    }
    let filename = args.get(1).unwrap();
    let file = File::open(filename);
    match file {
        Err(_) => {
            println!("Failed to open file '{}'", filename);
            std::process::exit(1); 
        }
        Ok(mut f) => {
            let mut content = vec!();
            f.read_to_end(&mut content).expect("Error while reading file");
            check_binary(&mut content);
            vm::execute(content)
        }
    }
}


// Check if binary is corrupted and exit on check failure
fn check_binary(binary: &mut Vec<u8>) {
    use std::collections::hash_map::DefaultHasher;
    use std::hash::{Hash, Hasher};
    let binary_checksum = binary.split_off(binary.len()-4);
    let mut hasher = DefaultHasher::new();
    binary.hash(&mut hasher);
    let computed_checksum = hasher.finish().to_le_bytes();
    if binary_checksum[..] != computed_checksum[..4] {
        eprintln!("Binary is corrupted, exiting.");
        std::process::exit(1);
    } 
}