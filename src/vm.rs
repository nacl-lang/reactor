const VERSION : [u8;2] = [1 as u8, 0 as u8];

struct VM {
    constants: Vec<[u8; 8]>,
    locals: Vec<Vec<[u8; 8]>>,
    globals: Vec<[u8; 8]>,
    operand_stack: Vec<[u8;8]>,
    call_stack: Vec<u16>,
    instructions: Vec<u8>, 
    pc: usize,
    max_locals: u8,
}

impl VM {
    fn new (bytecode: Vec<u8>) -> VM {
        let mut index = 0;
        // Check magic number
        if bytecode[index..6] != [78 as u8, 97 as u8, 67 as u8, 108 as u8, 11 as u8, 17 as u8] {
            eprintln!("Magic number did not match!");
            std::process::exit(1); 
        }
        index += 6;
        
        // Get version
        let version = [bytecode[index], bytecode[index+1]];
        // Check version
        if version != VERSION {
            eprintln!("Binary version ({}.{}) does not match VM version ({}.{})", version[0], version[1], VERSION[0], VERSION[1])     
        }
        index += 2;
        // Get max_local and max global
        let max_locals = bytecode[index];
        index += 1;
        let max_globals = bytecode[index];
        index += 1;

        // Initialize global variable stack
        let mut globals = Vec::with_capacity(max_globals as usize);
        for _i in 0..max_globals {
            globals.push(0u64.to_le_bytes()); 
        }

        // Get constants (First get constant count)
        let constant_count: u16 = u16::from_le_bytes([bytecode[index], bytecode[index+1]]);
        index += 2;
        let mut constants: Vec<[u8; 8]> = vec!();
        for _i in 0..constant_count {
            let mut help: [u8; 8] = [0; 8];
            for j in 0..8 {
                help[j] = bytecode[index];
                index += 1;
            }
            constants.push(help);
        }
        // Get Instructions
        let instructions: Vec<u8> = bytecode[index..bytecode.len()].to_vec();

        return VM {
            constants,
            locals: vec!(Vec::with_capacity(max_locals as usize)),
            globals, 
            operand_stack: Vec::with_capacity(1024),
            call_stack: Vec::with_capacity(256),
            instructions,
            pc: 0,
            max_locals,
        };
    }

    fn execute (&mut self) {
        macro_rules! bool_push {
            ($vm:expr, $e:expr) => {
                if $e {
                    $vm.operand_stack.push((-1i64).to_le_bytes())
                } else {
                    $vm.operand_stack.push(0i64.to_le_bytes())
                }
            };
        }

        macro_rules! float_bin_op {
            ($vm:expr, $op:tt) => {
                let second = f64::from_le_bytes($vm.operand_stack.pop().unwrap());
                let first = f64::from_le_bytes($vm.operand_stack.pop().unwrap());
                $vm.operand_stack.push((first $op second).to_le_bytes());
                $vm.pc += 1;
            };
        }

        macro_rules! integer_bin_op {
            ($vm:expr, $op:tt) => {
                let second = i64::from_le_bytes($vm.operand_stack.pop().unwrap());
                let first = i64::from_le_bytes($vm.operand_stack.pop().unwrap());
                $vm.operand_stack.push((first $op second).to_le_bytes());
                $vm.pc += 1;
            };
        }

        macro_rules! gp_bin_op {
            ($vm:expr, $op:tt) => {
                let second = u64::from_le_bytes($vm.operand_stack.pop().unwrap());
                let first = u64::from_le_bytes($vm.operand_stack.pop().unwrap());
                $vm.operand_stack.push((first $op second).to_le_bytes());
                $vm.pc += 1;
            };
        }

        macro_rules! float_cmp {
            ($vm:expr, $op:tt) => {
                let second = f64::from_le_bytes($vm.operand_stack.pop().unwrap());
                let first = f64::from_le_bytes($vm.operand_stack.pop().unwrap());
                bool_push!($vm, first $op second);
                $vm.pc += 1;
            };
        }

        macro_rules! integer_cmp {
            ($vm:expr, $op:tt) => {
                let second = i64::from_le_bytes($vm.operand_stack.pop().unwrap());
                let first = i64::from_le_bytes($vm.operand_stack.pop().unwrap());
                bool_push!($vm, first $op second);
                $vm.pc += 1;
            };
        }

        macro_rules! gp_cmp {
            ($vm:expr, $op:tt) => {
                let second = u64::from_le_bytes($vm.operand_stack.pop().unwrap());
                let first = u64::from_le_bytes($vm.operand_stack.pop().unwrap());
                bool_push!($vm, first $op second);
                $vm.pc += 1;
            };
        }
        loop {
            match self.instructions[self.pc] {
                0 => {
                    // FAdd
                    float_bin_op!(self, +); 
                }
                1 => {
                    // FSub
                    float_bin_op!(self, -); 
                }
                2 => {
                    // FMul
                    float_bin_op!(self, *); 
                }
                3 => {
                    // FDiv
                    float_bin_op!(self, /); 
                }
                4 => {
                    // FEq
                    float_cmp!(self, ==);
                }
                5 => {
                    // FNeq
                    float_cmp!(self, !=);
                }
                6 => {
                    // FG
                    float_cmp!(self, >);
                }
                7 => {
                    // FL
                    float_cmp!(self, <);
                }
                8 => {
                    // FGeq
                    float_cmp!(self, >=);
                }
                9 => {
                    // FLeq
                    float_cmp!(self, <=);
                }
                10 => {
                    // FNeg
                    let f = f64::from_le_bytes(self.operand_stack.pop().unwrap());
                    self.operand_stack.push((-f).to_le_bytes());
                    self.pc += 1;
                }
                11 => {
                    // IAdd
                    integer_bin_op!(self, +); 
                }
                12 => {
                    // ISub
                    integer_bin_op!(self, -); 
                }
                13 => {
                    // IMul
                    integer_bin_op!(self, *); 
                }
                14 => {
                    // IDiv
                    let second = i64::from_le_bytes(self.operand_stack.pop().unwrap());
                    let first = i64::from_le_bytes(self.operand_stack.pop().unwrap());
                    self.operand_stack.push((first as f64 / second as f64).to_le_bytes());
                    self.pc += 1;
                }
                15 => {
                    // IIDiv
                    integer_bin_op!(self, /); 
                }
                16 => {
                    // IMod
                    integer_bin_op!(self, %); 
                }
                17 => {
                    // IG
                    integer_cmp!(self, >); 
                }
                18 => {
                    // IL
                    integer_cmp!(self, <); 
                }
                19 => {
                    // IGeq
                    integer_cmp!(self, >=); 
                }
                20 => {
                    // ILeq
                    integer_cmp!(self, <=); 
                }
                21 => {
                    // INeg
                    let i = i64::from_le_bytes(self.operand_stack.pop().unwrap());
                    self.operand_stack.push((-i).to_le_bytes());
                    self.pc += 1;
                }
                22 => {
                    // And
                    gp_bin_op!(self,&);
                }
                23 => {
                    // Or
                    gp_bin_op!(self,|);
                }
                24 => {
                    // Eq
                    gp_cmp!(self,==);
                }
                25 => {
                    // Neq 
                    gp_cmp!(self,!=);
                }
                26 => {
                    // Not 
                    let i = u64::from_le_bytes(self.operand_stack.pop().unwrap());
                    self.operand_stack.push((!i).to_le_bytes());
                    self.pc += 1;
                }
                27 => {
                    // Pop
                    self.operand_stack.pop().unwrap();
                    self.pc += 1;
                }
                28 => {
                    // LoadC
                    let const_addr = u16::from_le_bytes([self.instructions[self.pc+1], self.instructions[self.pc+2]]) as usize;
                    self.operand_stack.push(self.constants[const_addr]);
                    self.pc += 3;
                }
                29 => {
                    // LoadL
                    let l_index = self.instructions[self.pc+1] as usize;
                    self.operand_stack.push(self.locals.last().unwrap().get(l_index).unwrap().clone());
                    self.pc += 2;
                }
                30 => {
                    // LoadG
                    let g_index = self.instructions[self.pc+1] as usize;
                    self.operand_stack.push(self.globals.get(g_index).unwrap().clone());
                    self.pc += 2;
                }
                31 => {
                    // StoreL
                    let l_index = self.instructions[self.pc+1] as usize;
                    let locals = self.locals.last_mut().unwrap();
                    locals[l_index] = self.operand_stack.pop().unwrap();
                    self.pc += 2;
                }
                32 => {
                    // StoreG
                    let g_index = self.instructions[self.pc+1] as usize;
                    self.globals[g_index] = self.operand_stack.pop().unwrap();
                    self.pc += 2;
                }
                33 => {
                    // CallF
                    let f_addr = u16::from_le_bytes([self.instructions[self.pc+1], self.instructions[self.pc+2]]) as usize;
                    self.call_stack.push((self.pc + 3) as u16);
                    let mut vector : Vec<[u8;8]>= Vec::with_capacity(self.max_locals as usize);
                    for _i in 0..self.max_locals {
                        vector.push([0;8]);
                    }
                    self.locals.push(vector);
                    self.pc = f_addr;
                }
                34 => {
                    // CallB
                    let b_id = self.instructions[self.pc+1] as usize;
                    match b_id {
                        0 => print!("{} ", i64::from_le_bytes(self.operand_stack.pop().unwrap())),
                        1 => print!("{} ", f64::from_le_bytes(self.operand_stack.pop().unwrap())),
                        2 => {
                            let val = i64::from_le_bytes(self.operand_stack.pop().unwrap());
                            let bool_val : bool;
                            if val == -1 {
                                bool_val = true;
                            } else if val == 0{
                                bool_val = false;
                            } else {
                                panic!("Tried to boolean print non-boolean value")
                            }
                            print!("{} ", bool_val)
                        }
                        3 => print!("\n"),
                        _ => panic!("Unknown builin function {}", b_id)
                    }
                    self.pc += 2;
                }
                35 => {
                    // Return
                    self.locals.pop();
                    self.pc = self.call_stack.pop().unwrap() as usize;
                }
                36 => {
                    // JmpU
                    let jmp_addr = u16::from_le_bytes([self.instructions[self.pc+1], self.instructions[self.pc+2]]) as usize;
                    self.pc = jmp_addr;
                }
                37 => {
                    // JmpT
                    let condition = i64::from_le_bytes(self.operand_stack.pop().unwrap());
                    if condition == -1 {
                        let jmp_addr = u16::from_le_bytes([self.instructions[self.pc+1], self.instructions[self.pc+2]]) as usize;
                        self.pc = jmp_addr;
                    } else {
                        self.pc += 3;
                    }
                }
                38 => {
                    // JmpF
                    let condition = i64::from_le_bytes(self.operand_stack.pop().unwrap());
                    if condition == 0 {
                        let jmp_addr = u16::from_le_bytes([self.instructions[self.pc+1], self.instructions[self.pc+2]]) as usize;
                        self.pc = jmp_addr;
                    } else {
                        self.pc += 3;
                    }
                }
                39 => {
                    // Exit
                    break;
                }
                _ => {
                    eprintln!("unknown instruction with id {} at address {}", self.instructions[self.pc], self.pc);
                    std::process::exit(1)
                }
            }
        }
    }
}

pub fn execute (bytecode: Vec<u8>){
    let mut vm = VM::new(bytecode);
    vm.execute();
    
}