# Reactor ![Reactor Icon](img/reactor_clear_128x.png "Reactor Icon") 

Reactor (short for "Molten Salt Reactor") is a Stack-based VM for the NaCl Programming language. Reactor interprets `.mltn` files, which are created from `.nacl` source files using the compiler in the [NaCl repository](https://gitlab.com/nacl-lang/nacl). Together with its sister project, it was written for the [Abstract Machines](https://www.complang.tuwien.ac.at/andi/185966) course at TU Wien.

## VM Layout

Reactor has a standard stack machine layout with an operator stack containing 8 byte values, that are cast to the right types according to the instruction that is executed. Furthermore, Reactor has a call stack containing the return pointer values of functions that are currently executed.

To store variables, Reactor has a global variable stack and a local variable stack (whose sizes are determined in the binary). Since the local variable stack must be kept seperate for each function, Reactor has a datastructure that handles function scoping such that recursive function calls do not interfere with each other.

The current address is stored in the `pc` field of the interpreter, which is incremented according to the executed instruction (this is necessarey due to the variable length instruction set).

## Binary Format

`.mltn` binaries are comprised as follows:
- 6 bytes magic number `NaCl\x11\x17` (11 and 17 are the atomic numbers of sodium and chlorine in the periodic table)
- 2 bytes version number (major version number, minor version number, currently 1.0)
- 1 byte local stack size 
- 1 byte global stack size 
- 2 bytes number of constants (little endian)
- constants (8 bytes each)
- instructions (variable length, see instruction set section)
- 4 byte checksum which are the (little endian) first four bytes of the default rust hash checksum calculated over the whole binary content

With this binary layout, `.mltn` binaries are typically very small and empty source files compile to a size of only 17 bytes. The binary of a (simple) example implementation of the `fib` function that prints a specified fibonacci number (see fib.nacl example in the NaCl repository) is only 99 bytes in size in the current compiler version (1.0).

### Restrictions for binaries 

The small `.mltn` binary format restricts the language to only 256 local and 256 global variables in scope at a time. We figured this should be enough since NaCl is mainly intended as a scripting language where the user normally doesn't have so many variables in scope at a time. If it turns out to be required, this restriction can easily be lifted in future versions.

Since constants are global and cannot go out of scope, NaCl allows for 65536 64-bit constants, which represent a value of an arbitrary type.

## Instruction Set

Reactor has a standard stack machine instruction set with four main types of instructions:

- Operator instructions for each data type (e.g. `FAdd`, `IMod`) that pop the topmost value(s) from the operand stack and push the result of the calculation to the operand stack
- Variable-related Instructions (e.g. `StoreL` to store stack content to local variable or `LoadC` to load a constant to the operand stack)
- Control flow Instructions (like `JmpU` (unconditional jump) or `Exit` (Exits the program))
- Function-Related instructions (`CallF` to call a function, `Return` to return from function, `CallB` to call builtin instruction)

For a detailed description of the instruction set, see [id.md](https://gitlab.com/nacl-lang/nacl/-/blob/master/dox/langspec.md) in the NaCl repository.

## Building and running Reactor
Reactor requires Rust 2018 to be compiled. Since it uses Rust's standard build tool `cargo`, Reactor can be compiled by running `cargo build --release`, which generates a `reactor` binary in the `target/release/` folder. Reactor takes a single command line argument which is the `.mltn` file it is supposed to run.

For debugging purposes, Reactor can also be run using `cargo run`